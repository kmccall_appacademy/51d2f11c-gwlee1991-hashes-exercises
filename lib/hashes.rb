# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  length = {}
  words = str.split
  words.each {|word| length[word] = word.length}
  length
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by {|k, v| v}[-1][0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  updated_inventory = older
  newer.each { |k, v| older[k] = v }
  updated_inventory
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  frequency = Hash.new(0)

  word.each_char {|ch| frequency[ch] += 1}

  frequency
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  unique = {}
  arr.each do |ele|
    unique[ele] = true
  end
  unique.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  even_odd = {even: 0, odd:0}

  numbers.each do |n|
    even_odd[:even] += 1 if n.even?
    even_odd[:odd] += 1 if n.odd?
  end

  even_odd
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowel = 'aeiou'.split('')
  vowel_count = Hash.new(0)

  string.each_char do |ch|
    vowel_count[ch] += 1 if vowel.include?(ch)
  end

  vowel_count.sort_by {|k, v| v}[-1][0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  pair = []

  selected_students = students.select {|name, month| month >= 7}
  names = selected_students.keys

  names.each_index do |i|
    ((i + 1)...names.length).each do |j|
      pair.push([names[i], names[j]])
    end
  end

  pair
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  diversity = specimens.uniq
  species_count = {}

  diversity.each do |species|
    species_count[species] = specimens.count(species)
  end

  number_of_species = diversity.length
  smallest_population_size = species_count.sort_by {|k, v| v}.first.last
  largest_population_size = species_count.sort_by{|k, v| v}.last.last

  number_of_species ** 2 * smallest_population_size / largest_population_size
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_sign_count = character_count(normal_sign)
  vandalized_sign_count = character_count(vandalized_sign)

  vandalized_sign_count.each do |k, v|
    return false if normal_sign_count[k.downcase] < vandalized_sign_count[k.downcase]
  end

  true
end

def character_count(str)
  count = Hash.new(0)

  str.each_char do |ch|
    next if ch == " "
    count[ch.downcase] += 1
  end

  count
end
